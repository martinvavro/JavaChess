/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.pjv.GameEngine;

import cz.cvut.fel.pjv.Moves.Bishop;
import cz.cvut.fel.pjv.Moves.King;
import cz.cvut.fel.pjv.Moves.Knight;
import cz.cvut.fel.pjv.Moves.Moves;
import cz.cvut.fel.pjv.Moves.Pawn;
import cz.cvut.fel.pjv.Moves.Queen;
import cz.cvut.fel.pjv.Moves.Rook;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author vior
 */
@RunWith(Parameterized.class)
public class FigureTest
{
    private final int position;
    private final int type;
    private final Moves expMove;
    private final boolean white;
    
    
    public FigureTest(int position, int type, Moves move, boolean white)
    {
	this.position = position;
	this.type = type;
	this.expMove = move;
	this.white = white;
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @Parameterized.Parameters
    public static Collection PrimeNumbers()
    {
	return Arrays.asList(new Object[][]
	{
	    {22,5,new King(),true},
	    {12,3,new Bishop(),false},
	    {15,2,new Knight(),true},
	    {35,4,new Queen(),false},
	    {88,0,new Pawn(),true},
	    {11,1,new Rook(),true},
	    {14,3,new Bishop(),true},
	    {44,5,new King(),true},
	});
    }

    
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of getPosition method, of class Figure.
     */
    @Test
    public void testGetPosition()
    {
	System.out.println("getPosition");
	Figure instance = new Figure(position, type, white);
	int expResult = position;
	int result = instance.getPosition();
    }

    /**
     * Test of getFigureID method, of class Figure.
     */
    @Test
    public void testGetFigureID()
    {
	System.out.println("getFigureID");
	Figure instance = new Figure(position, type, white);
	Integer expResult = type;
	Integer result = instance.getFigureID();
	assertEquals(expResult, result);
    }

    /**
     * Test of isWhite method, of class Figure.
     */
    @Test
    public void testIsWhite()
    {
	System.out.println("isWhite");
	Figure instance = new Figure(position, type, white);
	boolean expResult = white;
	boolean result = instance.isWhite();
	assertEquals(expResult, result);
    }


    /**
     * Test of setFigureType method, of class Figure.
     */
    @Test
    public void testSetFigureType()
    {
	System.out.println("setFigureType");
	Figure instance = new Figure(position, type, white);
	instance.setFigureType();
	assertEquals(instance.move.getClass(), expMove.getClass());
    }
    
}
