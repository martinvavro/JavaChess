/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.pjv.GameEngine;

import cz.cvut.fel.pjv.FrontEnd.DrawBoard;
import cz.cvut.fel.pjv.FrontEnd.InGameBar;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vior
 */
public class GameTest
{

    Board gameBoard;
    DrawBoard board;
    Pgn pgn;
    Game game;

    public GameTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp() throws IOException
    {
	gameBoard = new Board();
	pgn = new Pgn();
	board = new DrawBoard(new InGameBar(gameBoard), true, gameBoard);
	game = new Game();
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of calculateCheck method, of class Game.
     *
     * @throws java.io.IOException
     */
    @Test
    public void testCalculateCheck() throws IOException
    {
	System.out.println("calculateCheck");
//	assign
	gameBoard.defaultLayout();
	game.setKing(gameBoard);
	boolean expResult = false;
//	act
	boolean result = game.calculateCheck(gameBoard);
//	assert
	assertEquals(expResult, result);
    }

    /**
     * Test of isGameOver method, of class Game.
     */
    @Test
    public void testIsGameOver()
    {
	System.out.println("isGameOver");
//	assign
	gameBoard.defaultLayout();
	game.setKing(gameBoard);
	game.calculateThisTurnMoves(gameBoard);
	game.calculateCheck(gameBoard);
	boolean expResult = false;
//	act
	boolean result = game.isGameOver(pgn, gameBoard);
//	assert
	assertEquals(expResult, result);

    }

    /**
     * Test of getPossiblePositions method, of class Game.
     */
    @Test
    public void testGetPossiblePositions()
    {
	System.out.println("getPossiblePositions");
//	assign
	gameBoard.defaultLayout();
	game.setKing(gameBoard);
	game.calculateThisTurnMoves(gameBoard);
	game.calculateCheck(gameBoard);
	ArrayList<Integer> expResult = new ArrayList<>();
	expResult.add(12);
	expResult.add(17);
	expResult.add(21);
	expResult.add(22);
	expResult.add(23);
	expResult.add(24);
	expResult.add(25);
	expResult.add(26);
	expResult.add(27);
	expResult.add(28);
//	act
	ArrayList<Integer> result = game.getPossiblePositions();
//	assert
	assertEquals(expResult, result);

    }

}
