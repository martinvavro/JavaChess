package cz.cvut.fel.pjv.Moves;

import cz.cvut.fel.pjv.GameEngine.Board;


/**
 * Moves of a Rook.
 * @author vior
 */
public class Rook extends Moves
{
    int xAxis;

    /**
     * Calculates moves of a Rook.
     * @param gameBoard current figures positions
     * @param position requested position to calculate
     */
    @Override
    public void calculateMoves(Board gameBoard, int position)
    {
	originalPosition = position;
	move = originalPosition - 1;
	xAxis = originalPosition / 10;

//	calculate all possible moves left
	while (xAxis == move / 10)
	{
	    if (addMove(gameBoard, move) == false)
	    {
		if (addAttack(gameBoard, move) == true)
		{
		    break;
		}
		break;
	    }
	    move--;
	}

//	calculate all possible moves right
	move = originalPosition + 1;
	while (xAxis == move / 10 && move < 89)
	{
	    if (addMove(gameBoard, move) == false)
	    {
		if (addAttack(gameBoard, move) == true)
		{
		    break;
		}
		break;
	    }
	    move++;
	}

//	calculate all possible moves down
	move = originalPosition - 10;
	while (move > 10)
	{
	    if (addMove(gameBoard, move) == false)
	    {
		if (addAttack(gameBoard, move) == true)
		{
		    break;
		}
		break;
	    }
	    move -= 10;
	}

//	calculate all possible moves up
	move = originalPosition + 10;
	while (move < 89)
	{
	    if (addMove(gameBoard, move) == false)
	    {
		if (addAttack(gameBoard, move) == true)
		{
		    break;
		}
		break;
	    }
	    move += 10;
	}
    }
}
