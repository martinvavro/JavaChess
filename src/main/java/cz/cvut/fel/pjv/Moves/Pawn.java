/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.pjv.Moves;

import cz.cvut.fel.pjv.FrontEnd.PawnPromotion;
import cz.cvut.fel.pjv.GameEngine.Board;
import cz.cvut.fel.pjv.GameEngine.Figure;
import cz.cvut.fel.pjv.GameEngine.Status;
import javax.swing.JDialog;

/**
 * Moves of a Pawn.
 *
 * @author vior
 *
 */
public class Pawn extends Moves
{

    private int ep;

    /**
     * Calculates all possible moves of a pawn.
     *
     * @param gameBoard current figures positions
     * @param position requested position to calculate
     *
     */
    @Override
    public void calculateMoves(Board gameBoard, int position)
    {
	originalPosition = position;

//	calculate possible move 
	getMove(gameBoard, 10);
	if (addMove(gameBoard, move))

//	calculate first possible move
	{
	    if (!hasMoved)
	    {
		getMove(gameBoard, 20);
		if (addMove(gameBoard, move))
		{
		    ep = move;
		}
	    }
	}

//	Check if attack can be made on left side
	getMove(gameBoard, 11);
	addAttack(gameBoard, move);

//	Check if attack is possible on right side
	getMove(gameBoard, 9);
	addAttack(gameBoard, move);

//	check if en passable is possible on right side
	move = originalPosition + 1;
	calculateEp(gameBoard);

	move = originalPosition - 1;
	calculateEp(gameBoard);

    }

    /**
     * Moves pawn to a requested position if possible.
     *
     * @param gameBoard current figures positions
     * @param position requested position to calculate
     * @return true if move has been made
     */
    @Override
    public boolean commitMove(Board gameBoard, int position)
    {
	if (moves.contains(position))
	{
	    gameBoard.switchPosition(originalPosition, position);
	    if (!justCheck)
	    {
		enpasableTurn = false;
		changeFigure(gameBoard, position);
	    }
	    if (position == ep)
	    {
		if (!justCheck)
		{
		    enPass = true;
		    enpasableTurn = true;
		}
	    }
	    hasMoved = true;
	    Status.resetFiftyMoveRule();
	    return true;
	}

	if (attacks.contains(position))
	{
	    if (position == enPassPosition)
	    {
		gameBoard.attackPosition(originalPosition, position);
		if (gameBoard.findByPosition(position).isWhite())
		{
		    gameBoard.attackPosition(position, position + 10);
		    position += 10;
		}
		else
		{
		    gameBoard.attackPosition(position, position - 10);
		    position -= 10;
		}
	    }
	    else
	    {
		gameBoard.attackPosition(originalPosition, position);
	    }
	    hasMoved = true;
	    if (!justCheck)
	    {
		changeFigure(gameBoard, position);
	    }
	    Status.resetFiftyMoveRule();
	    if (!justCheck)
	    {
		wasAttack = true;
	    }
	    enpasableTurn = false;
	    return true;
	}
	return false;
    }

    private void getMove(Board gameBoard, int factor)
    {
	if (gameBoard.findByPosition(originalPosition) != null)
	{
	    if (gameBoard.findByPosition(originalPosition).isWhite())
	    {
		move = originalPosition + factor;
	    }
	    else
	    {
		move = originalPosition - factor;
	    }
	}
    }

    private void changeFigure(Board gameBoard, int position)
    {
	if (gameBoard.findByPosition(position) != null)
	{
	    if (gameBoard.findByPosition(position).getFigureID() == 0)
	    {
		if (gameBoard.findByPosition(position).isWhite() && position / 10 == 8)
		{
		    JDialog pp = new PawnPromotion(position, true, gameBoard);
		    pp.setModal(true);
		    pp.setVisible(true);
		    if (Status.isAiGame() && !Status.isWhiteTurn())
		    {
			pp.setVisible(false);
			pp.dispose();
		    }
		    gameBoard.findByPosition(position).move.pawnPromotionFigure = gameBoard.findByPosition(position).getFigureID();
		    gameBoard.findByPosition(position).move.pawnPromotion = true;
		}
		else if (!gameBoard.findByPosition(position).isWhite() && position / 10 == 1)
		{
		    JDialog pp = new PawnPromotion(position, false, gameBoard);
		    pp.setModal(true);
		    pp.setVisible(true);
		    if (Status.isAiGame() && !Status.isWhiteTurn())
		    {
			pp.setVisible(false);
			pp.dispose();
		    }
		    gameBoard.findByPosition(position).move.pawnPromotionFigure = gameBoard.findByPosition(position).getFigureID();
		    gameBoard.findByPosition(position).move.pawnPromotion = true;
		}
	    }
	}
    }

    private void calculateEp(Board gameBoard)
    {
	Figure enPassante = gameBoard.findByPosition(move);
	if (enPassante != null)
	{
	    if (enPassante.move.enPass == true)
	    {
		if (enPassante.isWhite() != gameBoard.findByPosition(originalPosition).isWhite())
		{
		    attacks.add(move);
		    enPassPosition = move;
		}

	    }
	}
    }
}
