/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.pjv.Moves;

import cz.cvut.fel.pjv.GameEngine.Board;



/**
 * 
 * Moves of a bishop
 * @author vior
 */
public class Bishop extends Moves
{
    
    /**
     * Calculates moves
     * @param gameBoard current figures positions
     * @param position requested position to calculate
     * 
     */
    @Override
    public void calculateMoves(Board gameBoard, int position)
    {
	int factor;
	originalPosition = position;

//	calculate all possible moves down left
	factor = -11;
	checkPossibleMoveLoop(gameBoard, factor);
	
	factor = 9;
	checkPossibleMoveLoop(gameBoard, factor);
	
	factor = - 9;
	checkPossibleMoveLoop(gameBoard, factor);
	
	factor = 11;
	checkPossibleMoveLoop(gameBoard, factor);
    }
    
    private void checkPossibleMoveLoop(Board gameBoard, int factor)
    {
	move = originalPosition + factor;
	while (move > 10 && move % 10 != 0 && move < 89 && move % 10 != 9)
	{
	    if (addMove(gameBoard, move) == false)
	    {
		if (addAttack(gameBoard, move) == true)
		{
		    break;
		}
		break;
	    }
	    addMove(gameBoard, move);
	    move += factor;
	}
    }
}
