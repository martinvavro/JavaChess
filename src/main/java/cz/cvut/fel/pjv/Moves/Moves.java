
/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.pjv.Moves;

import cz.cvut.fel.pjv.FrontEnd.DrawBoard;
import cz.cvut.fel.pjv.GameEngine.Board;
import cz.cvut.fel.pjv.GameEngine.Game;
import cz.cvut.fel.pjv.GameEngine.Status;
import java.util.ArrayList;

/**
 * Contains of move operations.
 *
 * @author vior
 *
 */
public class Moves
{

    /**
     * Collection of a multiple variables used across all figure types.
     */
    ArrayList<Integer> moves;
    ArrayList<Integer> attacks;
    int originalPosition;
    int move;
    boolean enPass = false;
    boolean hasMoved;
    int enPassPosition;
    boolean leftCastle;
    boolean rightCastle;
    static boolean justCheck;
    boolean leftCastleCommited;
    boolean rightCastleCommited;
    boolean wasAttack;
    static boolean enpasableTurn;
    boolean pawnPromotion;
    int pawnPromotionFigure;

    /**
     *
     * Constructor of figure Moves
     */
    public Moves()
    {
	this.attacks = new ArrayList<>();
	this.moves = new ArrayList<>();
    }

    /**
     * Overridden method for calculating moves.
     *
     * @param gameBoard current figures positions
     * @param position requested position to calculate
     */
    public void calculateMoves(Board gameBoard, int position)
    {
    }

    /**
     * Moves figure to a requested position if possible.
     *
     * @param gameBoard current figures positions
     * @param position requested position to calculate
     * @return true if move has been made
     */
    public boolean commitMove(Board gameBoard, int position)
    {
	if (moves.contains(position))
	{
	    gameBoard.switchPosition(originalPosition, position);
	    hasMoved = true;
	    Status.addFiftyMoveRule();
	    if (!justCheck)
	    {
		enpasableTurn = false;
	    }
	    return true;
	}

	if (attacks.contains(position))
	{
	    gameBoard.attackPosition(originalPosition, position);
	    hasMoved = true;
	    Status.resetFiftyMoveRule();
	    if (!justCheck)
	    {
		wasAttack = true;
		enpasableTurn = false;
	    }
	    return true;
	}

	return false;
    }

    /**
     * Determines whether move or an attack is possible.
     *
     * @param gameBoard current figures positions
     */
    public void checkPossibleMove(Board gameBoard)
    {
	if (!addMove(gameBoard, move))
	{
	    addAttack(gameBoard, move);
	}
    }

    /**
     * Draws border of a button to corresponding color if move/attack/castle is
     * possible.
     */
    public void drawPossibilities()
    {
//	Draw blue frame when castle is possible
	if (leftCastle)
	{
	    DrawBoard.getButtons().get(originalPosition - 2).setBorderCastle();
	}
	if (rightCastle)
	{
	    DrawBoard.getButtons().get(originalPosition + 2).setBorderCastle();
	}

//	Draw green frame on accesible squares
	moves.forEach((Integer helper) ->
	{
	    DrawBoard.getButtons().get(helper).setBorderAccessible();
	});

//	Draw red frame on attackable squares
	attacks.forEach((Integer helper) ->
	{
	    DrawBoard.getButtons().get(helper).setBorderAttackable();
	});
    }

    /**
     * Calculates whether move can be made on a tile in parameter.
     *
     * @param gameBoard current figures positions
     * @param move position of a tile to check
     * @return true if move is possible on requested tile
     */
    public boolean addMove(Board gameBoard, int move)
    {
	if (move > 10 && move < 89)
	{
	    if (DrawBoard.getButtons().get(move) != null)
	    {
		if (gameBoard.findByPosition(move) == null)
		{
		    moves.add(move);

		    if (!justCheck)
		    {
			checkCheck(move, gameBoard);
		    }
		    return true;
		}
	    }
	}
	return false;
    }

    /**
     * Calculates whether attack can be made on a tile in parameter.
     *
     * @param gameBoard current figures positions
     * @param move position of a tile to check
     * @return true if attack is possible on requested tile
     */
    public boolean addAttack(Board gameBoard, int move) 
    {
	if (move > 10 && move < 89)
	{
	    if (DrawBoard.getButtons().get(move) != null)
	    {
		if (gameBoard.findByPosition(move) != null)
		{
		    if (gameBoard.findByPosition(originalPosition).isWhite() != gameBoard.findByPosition(move).isWhite())
		    {
			attacks.add(move);

			if (!justCheck)
			{
			    checkCheck(move, gameBoard);
			}
			return true;
		    }
		}
	    }
	}
	return false;
    }

    /**
     *
     * Returns a list of all possible moves of a figure.
     *
     * @return a list of all possible moves of a figure
     */
    public ArrayList<Integer> getMoves()
    {
	return moves;
    }

    /**
     * Returns a list of all possible attacks of a figure
     *
     * @return a list of all possible attacks of a figure
     */
    public ArrayList<Integer> getAttacks()
    {
	return attacks;
    }

    /**
     * Returns true if figure has moved.
     *
     * @return true if figure has moved
     */
    public boolean isHasMoved()
    {
	return hasMoved;
    }

    /**
     * Set true if figure has already moved.
     *
     * @param hasMoved true if figure has already moved
     */
    public void setHasMoved(boolean hasMoved)
    {
	this.hasMoved = hasMoved;
    }

    /**
     * Deletes all moves of a figure.
     */
    public void deleteMoves()
    {
	moves.removeAll(moves);
    }

    /**
     * Deletes all attacks of a figure.
     */
    public void deleteAttacks()
    {
	attacks.removeAll(attacks);

    }

    /**
     * Removes single move.
     *
     * @param move coordinates of a move to delete.
     */
    public void removeMove(int move)
    {
	moves.remove(move);
    }

    /**
     * Deletes all static variables values so it won't interfere in next move
     * calculations.
     */
    public void cleanAll()
    {
	deleteMoves();
	deleteAttacks();
	leftCastle = false;
	rightCastle = false;
	leftCastleCommited = false;
	rightCastleCommited = false;
	wasAttack = false;
	pawnPromotion = false;
	if (!enpasableTurn)
	{
	    enPass = false;
	    enPassPosition = 0;
	}
    }

    /**
     * If castle was committed this turn returns true.
     *
     * @return true if castle was committed this turn.
     */
    public boolean isLeftCastleCommited()
    {
	return leftCastleCommited;
    }

    /**
     * If castle was committed this turn returns true.
     *
     * @return true if castle was committed this turn.
     */
    public boolean isRightCastleCommited()
    {
	return rightCastleCommited;
    }

    /**
     * Returns true if this turn committed move was attack.
     *
     * @return true if this turn committed move was attack
     */
    public boolean isWasAttack()
    {
	return wasAttack;
    }

    /**
     * True to prevent infinite recursion when calculating this turn check.
     *
     * @param justCheck true to prevent infinite recursion when calculating this
     * turn check
     */
    public static void setJustCheck(boolean justCheck)
    {
	Moves.justCheck = justCheck;
    }

    /**
     * Returns true to prevent infinite recursion when calculating this turn
     * check.
     *
     * @return true to prevent infinite recursion when calculating this turn
     * check
     */
    public static boolean isJustCheck()
    {
	return justCheck;
    }

    /**
     * Returns true if this turn committed move was pawn promotion.
     *
     * @return true if this turn committed move was pawn promotion
     */
    public boolean isPawnPromotion()
    {
	return pawnPromotion;
    }

    /**
     * Returns the position of a figure that underwent pawn promotion.
     *
     * @return the position of a figure that underwent pawn promotion.
     */
    public int getPawnPromotionFigure()
    {
	return pawnPromotionFigure;
    }

    /**
     * My favorite function name. Checks if king is in check. Commits move to
     * add in new board and if it is illegal it gets deleted.
     *
     * @param move position of a move in question
     */
    private void checkCheck(int move, Board gameBoard)
    {
	boolean previousMove = hasMoved;
	Game game = new Game();
	game.setKing(gameBoard);
//	deep copy gameBoard
	Board checkBoard = gameBoard.cloneBoard();
//	remember moves made before check so that the game can return to previous state
	int fifty = Status.getFiftyMoveRule();
//	flag for add move and attack, so this function wont be called again	
	justCheck = true;
//	this will move the figure on the position move
	commitMove(checkBoard, move);
//	this will run a function which will compute all the moves of the enemy
//	and returns true if kings position is compromised
	if (checkBoard.findByPosition(move).getFigureID() == 5)
	{
	    game.setKing(checkBoard);
	}
	if (game.calculateCheck(checkBoard))
	{
	    if (attacks.contains(move))
	    {
		attacks.remove(attacks.size() - 1);
	    }
	    if (moves.contains(move))
	    {
		moves.remove(moves.size() - 1);
	    }
	}
//	return variables to previous state
	hasMoved = previousMove;
	justCheck = false;
	Status.setFiftyMoveRule(fifty);
    }

}
