package cz.cvut.fel.pjv.Moves;

import cz.cvut.fel.pjv.GameEngine.Board;
import cz.cvut.fel.pjv.GameEngine.Status;

/**
 * Moves of a king.
 *
 * @author vior
 *
 */
public class King extends Moves
{

    /**
     * Calculates moves.
     *
     * @param gameBoard current figures positions
     * @param position requested position to calculate
     */
    @Override
    public void calculateMoves(Board gameBoard, int position)
    {
	originalPosition = position;

//	Checks the possibility of a move in king's radius
	move = originalPosition + 11;
	checkPossibleMove(gameBoard);

	move = originalPosition + 10;
	checkPossibleMove(gameBoard);

	move = originalPosition + 9;
	checkPossibleMove(gameBoard);

	move = originalPosition + 1;
	checkPossibleMove(gameBoard);

	move = originalPosition - 1;
	checkPossibleMove(gameBoard);

	move = originalPosition - 11;
	checkPossibleMove(gameBoard);

	move = originalPosition - 9;
	checkPossibleMove(gameBoard);

	move = originalPosition - 10;
	checkPossibleMove(gameBoard);
//	new king position cant be checked
	if (!Status.isCheck())
	{
	    calculateCastle(gameBoard);
	}
    }

    /**
     * Moves figure to a requested position if possible.
     *
     * @param gameBoard current figures positions
     * @param position requested position to move to
     *
     * @return true if move has been made
     */
    @Override
    public boolean commitMove(Board gameBoard, int position)
    {
	if (!Status.isCheck())
	{
	    if (position == originalPosition + 2 && rightCastle)
	    {
		gameBoard.switchPosition(originalPosition, position);
		gameBoard.switchPosition(originalPosition + 3, originalPosition + 1);
		rightCastleCommited = true;
		Status.addFiftyMoveRule();
		return true;
	    }
	    else if (position == originalPosition - 2 && leftCastle)
	    {
		gameBoard.switchPosition(originalPosition, position);
		gameBoard.switchPosition(originalPosition - 4, originalPosition - 1);
		Status.addFiftyMoveRule();
		leftCastleCommited = true;
		return true;
	    }
	}

	return super.commitMove(gameBoard, position);
    }

    // I'm sorry..
    private void calculateCastle(Board gameBoard)
    {
	if (!hasMoved)
	{
	    if (gameBoard.findByPosition(originalPosition + 1) == null)
	    {
		if (gameBoard.findByPosition(originalPosition + 2) == null)
		{
		    if (gameBoard.findByPosition(originalPosition + 3) != null)
		    {
			if (gameBoard.findByPosition(originalPosition + 3).getFigureID() == 1 && !gameBoard.findByPosition(originalPosition + 3).move.hasMoved)
			{
			    addMove(gameBoard, originalPosition + 2);
			    if (moves.contains(originalPosition + 2))
			    {
				rightCastle = true;
				moves.remove(moves.size() - 1);
			    }
			}
		    }
		}
	    }
	    if (gameBoard.findByPosition(originalPosition - 1) == null)
	    {
		if (gameBoard.findByPosition(originalPosition - 2) == null)
		{
		    if (gameBoard.findByPosition(originalPosition - 3) == null)
		    {
			if (gameBoard.findByPosition(originalPosition - 4) != null)
			{
			    if (gameBoard.findByPosition(originalPosition - 4).getFigureID() == 1 && !gameBoard.findByPosition(originalPosition - 4).move.hasMoved)
			    {
				addMove(gameBoard, originalPosition - 2);
				if (moves.contains(originalPosition - 2))
				{
				    leftCastle = true;
				    moves.remove(moves.size() - 1);
				}
			    }
			}
		    }
		}
	    }
	}
    }
}
