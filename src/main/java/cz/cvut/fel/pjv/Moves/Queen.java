/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.pjv.Moves;

import cz.cvut.fel.pjv.GameEngine.Board;

/**
 * Moves of a Queen.
 *
 * @author vior
 */
public class Queen extends Moves
{

    Rook rookMoves;
    Bishop bishopMoves;

    /**
     * Queen is just a combination of rook and bishop moves. I created instances
     * of them both. Theirs moves gets assigned to queen.
     */
    public Queen()
    {
	this.rookMoves = new Rook();
	this.bishopMoves = new Bishop();
    }

    /**
     * Calculates moves of a Bishop and Rook and then assigns them to Queen.
     *
     * @param gameBoard current figures positions
     * @param position requested position to calculate
     */
    @Override
    public void calculateMoves(Board gameBoard, int position)
    {
//	remove all moves of subclasses
	rookMoves.moves.removeAll(rookMoves.moves);
	bishopMoves.moves.removeAll(bishopMoves.moves);
	rookMoves.attacks.removeAll(rookMoves.attacks);
	bishopMoves.attacks.removeAll(bishopMoves.attacks);

	originalPosition = position;

	rookMoves.calculateMoves(gameBoard, position);

	moves.addAll(rookMoves.moves);
	attacks.addAll(rookMoves.attacks);

	bishopMoves.calculateMoves(gameBoard, position);

	moves.addAll(bishopMoves.moves);
	attacks.addAll(bishopMoves.attacks);
    }

    /**
     * Moves queen to a requested position if possible.
     *
     * @param gameBoard current figures positions
     * @param position requested position to move to
     * @return true if move has been made
     */
    @Override
    public boolean commitMove(Board gameBoard, int position)
    {
	boolean returnValue;
	if (!rookMoves.commitMove(gameBoard, position))
	{
	    returnValue = bishopMoves.commitMove(gameBoard, position);
	}
	else
	{
	    returnValue = true;
	}
	if (bishopMoves.wasAttack || rookMoves.wasAttack )
	{
	    this.wasAttack = true;
	}
	return returnValue;
    }
}
