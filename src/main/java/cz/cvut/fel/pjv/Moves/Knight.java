/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.pjv.Moves;

import cz.cvut.fel.pjv.GameEngine.Board;


/**
 * Moves of a Knight.
 * @author vior
 * 
 */
public class Knight extends Moves
{

    /**
     * Calculates all possible moves of a knight.
     * @param gameBoard current figures positions
     * @param position requested position to calculate
     * 
     */
    @Override
    public void calculateMoves(Board gameBoard, int position)
    {
//	calculate all possible moves left
	originalPosition = position;
	
	move = originalPosition + 21;
	checkPossibleMove(gameBoard);
	
	move = originalPosition + 19;
	checkPossibleMove(gameBoard);
	
	move = originalPosition + 8;
	checkPossibleMove(gameBoard);
	
	move = originalPosition + 12;
	checkPossibleMove(gameBoard);
	
	move = originalPosition - 19;
	checkPossibleMove(gameBoard);
	
	move = originalPosition - 12;
	checkPossibleMove(gameBoard);
	
	move = originalPosition - 8;
	checkPossibleMove(gameBoard);
	
	move = originalPosition - 21;
	checkPossibleMove(gameBoard);
    }

}
