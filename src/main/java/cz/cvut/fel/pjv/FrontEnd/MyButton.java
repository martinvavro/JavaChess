package cz.cvut.fel.pjv.FrontEnd;

import cz.cvut.fel.pjv.GameEngine.Board;
import cz.cvut.fel.pjv.GameEngine.Figure;
import java.awt.Color;
import java.awt.Image;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Each button represents chess board tile. 
 * This method extends JButton so that we can change button icon and border 
 * in a single function.
 * @author vior
 * 
 */
public class MyButton extends JButton
{
    private int id;
    
    /**
     * Returns button id which also corresponds to it's coordinates.
     * @return button id which also corresponds to it's coordinates.
     */
    private int getId()
    {
	return id;
    }

    /**
     * Constructor.
     * @param action action command is used to 
     * identify button coordinates in button handler.
     */
    public MyButton(Action action)
    {
	super(action);
    }

    /**
     * 
     * @param id corresponds to button coordinates
     */
    public void setId(int id)
    {
	this.id = id;
    }

    /**
     * Sets border of a button to green color.
     */
    public void setBorderAccessible()
    {

	this.setBorder(BorderFactory.createLineBorder(Color.green, 3));
    }
    
    /**
     * Sets border of a button to blue color.
     */
    public void setBorderCastle()
    {
//	Line Border + Thickness of the Border.
	this.setBorder(BorderFactory.createLineBorder(Color.blue, 3));
    }

    /**
     * Sets border of a button to red color.
     */
    public void setBorderAttackable()
    {
	this.setBorder(BorderFactory.createLineBorder(Color.RED, 3));
    }

    /**
     * Removes button border.
     */
    public void removeBorder()
    {
	this.setBorder(null);
    }

    /**
     * Based on the figure ID draws an corresponding icon.
     * @throws IOException in case icon is not found.
     * 
     */
    public void drawIcon(Board gameBoard) throws IOException
    {
	Image img = null;
	Figure figure = gameBoard.findByPosition(this.id);
	if (figure != null)
	{
	    if (figure.getFigureID() != null)
	    {
		switch (figure.getFigureID())
		{
		    case 0:
			img = pickColor(figure, "Pawn");
			break;
		    case 1:
			img = pickColor(figure, "Rook");
			break;
		    case 2:
			img = pickColor(figure, "Knight");
			break;
		    case 3:
			img = pickColor(figure, "Bishop");
			break;
		    case 4:
			img = pickColor(figure, "Queen");
			break;
		    case 5:
			img = pickColor(figure, "King");
			break;
		    default:
		    {
			this.setIcon(null);
			break;
		    }
		}
	    }
	    this.setIcon(new ImageIcon(img));
	}
	else
	{
	    this.setIcon(null);
	}
    }

    private Image pickColor(Figure figure, String image) throws IOException
    {
	Image img;
	if (figure.isWhite())
	{
	    img = ImageIO.read(getClass().getClassLoader().getResource(image + "White.png"));
	}
	else
	{
	    img = ImageIO.read(getClass().getClassLoader().getResource(image + "Black.png"));
	}
	return img;
    }
}
