package cz.cvut.fel.pjv.FrontEnd;

import cz.cvut.fel.pjv.GameEngine.Board;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 * Non trivial Swing JPanel which spawns buttons in grid layout.
 * Each button is identified by it's action command which corresponds to it's position.
 * @author vior
 * 
 */
public class DrawBoard extends JPanel
{

    private static ArrayList<MyButton> buttons = new ArrayList<MyButton>(89);

    /**
     * Constructor for DrawBoard, draws new chess board.
     * @param ig to control current time indicator.
     * @param isGame if true, use this for game, if false 
     * use this to create custom figure layout.
     * @param gameBoard  current board
     * @throws IOException
     */
    public DrawBoard(InGameBar ig, boolean isGame, Board gameBoard) throws IOException
    {
//	 Color of chess board this you can change accordingly like red or white
	Color blackColor = Color.GRAY;
	Color whiteColor = Color.WHITE;
//	fill the arraylist with null values
	for (int i = 0; i < 89; i++)
	{
	    buttons.add(null);
	}
	MyButton chessButton;
	ButtonHandler buttonID = new ButtonHandler(gameBoard);
	SetFigureButtonHandler buttonIDS = new SetFigureButtonHandler(gameBoard);
	if (isGame)
	{
	    buttonID.setIngameMenu(ig);
	}

	for (int j = 1; j <= 8; j++)
	{
	    for (int i = 1; i <= 8; i++)
	    {
		String poString = String.valueOf(9 - j) + String.valueOf(i);
		Integer position = Integer.valueOf(poString);
		if (isGame)
		{
		    chessButton = new MyButton(buttonID);
		}
		else
		{
		    chessButton = new MyButton(buttonIDS);
		}
		chessButton.setId(position);
		chessButton.setActionCommand(poString);
		if (i % 2 == 0)
		{ // Adding color based on the odd and even initially.  
		    chessButton.setBackground(blackColor);
		}
		else
		{
		    chessButton.setBackground(whiteColor);
		}
		add(chessButton);

		chessButton.drawIcon(gameBoard);

		buttons.set(position, chessButton);
		if (i % 8 == 0)
		{ // swapping the color when adding the next row 
		    Color temp = blackColor;
		    blackColor = whiteColor;
		    whiteColor = temp;
		}
	    }
	}
	this.setLayout(new GridLayout(8, 8)); // GridLayout will arrange elements in Grid Manager 8 X 8
	this.setSize(650, 650); // Size of the chess board
	this.setMaximumSize(new Dimension(650, 650));
	this.setMinimumSize(new Dimension(650, 650));
	this.setVisible(true);
    }

    /**
     * Returns all buttons created.
     * @return all buttons created
     */
    public static ArrayList<MyButton> getButtons()
    {
	return buttons;
    }
    
    /**
     * Redraws icons on buttons so that player
     * can see which figure is on the button.
     * @param gameBoard current board
     */
    public static void redrawBoard(Board gameBoard)
    {
	buttons.forEach((MyButton helper) ->
	{
	    if (helper != null)
	    {
		helper.removeBorder();
		try
		{
		    helper.drawIcon(gameBoard);
		}
		catch (IOException ex)
		{
		    Logger.getLogger(DrawBoard.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	});
    }

}
