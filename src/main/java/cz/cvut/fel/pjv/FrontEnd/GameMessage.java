/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package cz.cvut.fel.pjv.FrontEnd;


import cz.cvut.fel.pjv.GameEngine.Board;
import cz.cvut.fel.pjv.GameEngine.Pgn;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

/**
 * Informs the user about game status change - Stalemate, Checkmate. 
 * @author vior
 */
public class GameMessage extends javax.swing.JDialog
{
    private final Pgn pgn;
    private final Board gameBoard;
    /**
     * Constructor for GameMessage.
     * @param parent parent JFrame to spawn this above
     * @param modal whether it is modal
     * @param gameBoard
     * @param pgn
     */
    public GameMessage(java.awt.Frame parent, boolean modal, Board gameBoard, Pgn pgn)
    {
	super(parent, modal);
	initComponents();
	this.gameBoard = gameBoard;
	this.pgn = pgn;
    }
    
    
    /**
     * Sets JLabel to String info value.
     * @param info String which gets printed on the screen
     */
    public void printMessage(String info)
    {
	message.setText(info);
	message.setHorizontalAlignment(SwingConstants.CENTER);
    }
	    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        exit = new javax.swing.JButton();
        message = new javax.swing.JLabel();
        showPGN = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setMaximumSize(new java.awt.Dimension(400, 300));

        exit.setText("Exit to Main Menu");
        exit.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        exit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                exitActionPerformed(evt);
            }
        });
        getContentPane().add(exit, java.awt.BorderLayout.PAGE_END);

        message.setMaximumSize(new java.awt.Dimension(350, 100));
        message.setMinimumSize(new java.awt.Dimension(350, 100));
        message.setPreferredSize(new java.awt.Dimension(350, 100));
        getContentPane().add(message, java.awt.BorderLayout.PAGE_START);

        showPGN.setText("Show PGN notation");
        showPGN.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                showPGNActionPerformed(evt);
            }
        });
        getContentPane().add(showPGN, java.awt.BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void exitActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_exitActionPerformed
    {//GEN-HEADEREND:event_exitActionPerformed
        this.setVisible(false);
	this.dispose();
	MainWindow.getWindow().removeAll();
	MainWindow.getWindow().dispose();
	MainWindow.drawWindow(gameBoard); 
    }//GEN-LAST:event_exitActionPerformed

    private void showPGNActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_showPGNActionPerformed
    {//GEN-HEADEREND:event_showPGNActionPerformed
	message.setVisible(false);
	JTextArea messagePGN = new JTextArea();
	JScrollPane sp = new JScrollPane(messagePGN);
	sp.setBounds(0, 0, 400, 175);
	sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
//	messagePGN.setColumns(25);
	messagePGN.setLineWrap(true);
	showPGN.setVisible(false);
	
	messagePGN.setText(pgn.getPgn());
	getContentPane().add(sp, java.awt.BorderLayout.CENTER);
	sp.setVisible(true);
	messagePGN.setBackground(Color.WHITE);
	messagePGN.setForeground(Color.BLACK);
    }//GEN-LAST:event_showPGNActionPerformed



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton exit;
    private javax.swing.JLabel message;
    private javax.swing.JButton showPGN;
    // End of variables declaration//GEN-END:variables
}
