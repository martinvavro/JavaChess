/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.pjv.FrontEnd;

import cz.cvut.fel.pjv.GameEngine.Board;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 * Button handler when creating custom figure layout. Calls PickFigure.
 *
 * @author vior
 *
 */
public class SetFigureButtonHandler extends AbstractAction
{

    private final Board gameBoard;
    private static int setButtonPressed;

    public SetFigureButtonHandler(Board gameBoard)
    {
	this.gameBoard = gameBoard;
    }
    
    

    @Override
    public void actionPerformed(ActionEvent e)
    {
//	This will get the pressed button coordinates
	setButtonPressed = Integer.valueOf(e.getActionCommand());
	PickFigure pick = new PickFigure(gameBoard);
	pick.setVisible(true);
    }

    /**
     * Returns current pressed button.
     * @return returns current pressed button
     */
    public static int getSetButtonPressed()
    {
	return setButtonPressed;
    }
    
    
}
