/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.pjv.FrontEnd;

import cz.cvut.fel.pjv.GameEngine.Board;
import cz.cvut.fel.pjv.GameEngine.Game;
import cz.cvut.fel.pjv.GameEngine.IO;
import cz.cvut.fel.pjv.GameEngine.Loop;
import cz.cvut.fel.pjv.GameEngine.Status;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 * InGameBar keeps track of both players time left, current player color and
 * handles exiting game which includes option to save current game.
 *
 * @author vior
 */
public class InGameBar extends javax.swing.JPanel implements Runnable
{

    private int whiteClock;
    private int blackClock;
    private Board gameBoard;

    /**
     * Creates new form IngameMenu, starts timer if defined.
     */
    public InGameBar(Board gameBoard)
    {
	this.gameBoard = gameBoard;
	initComponents();
	if (Status.isWhiteTurn())
	{
	    jButton2.setBackground(Color.WHITE);
	}
	else
	{
	    jButton2.setBackground(Color.BLACK);
	}
    }

    /**
     * Changes the color of a tile which is current player turn color.
     *
     * @param white true for white false for black
     */
    public void setBackGround(boolean white)
    {
	Color c = white ? Color.WHITE : Color.BLACK;
	jButton2.setBackground(c);
    }

    /**
     * Starts new Thread with chess clock if defined.
     */
    public void setClock()
    {
	if (Status.isChessClock())
	{
	    whiteClock = Status.getTimeLimit();
	    blackClock = Status.getTimeLimit();
	    blackTimeLeft.setText(printTime(Status.getTimeLimit()));
	    whiteTimeLeft.setText(printTime(Status.getTimeLimit()));
	    Thread timeThread = new Thread(this);
	    timeThread.start();
	}
	else
	{
	    blackTimeBox.setVisible(false);
	    whiteTimeBox.setVisible(false);
	}
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        areYouSure = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        noDontSave = new javax.swing.JButton();
        yesSave = new javax.swing.JButton();
        cancel = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        blackTimeBox = new javax.swing.JPanel();
        blackTimeLeft = new javax.swing.JTextField();
        whiteTimeBox = new javax.swing.JPanel();
        whiteTimeLeft = new javax.swing.JTextField();
        CurrentTurn = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();

        areYouSure.setTitle("Are you sure");
        areYouSure.setAlwaysOnTop(true);
        areYouSure.setResizable(false);
        areYouSure.setLocationRelativeTo(this);
        areYouSure.setSize(new java.awt.Dimension(385, 114));

        jLabel1.setText("Do you want to save this game?");

        noDontSave.setText("No");
        noDontSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                noDontSaveActionPerformed(evt);
            }
        });

        yesSave.setText("Yes");
        yesSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                yesSaveActionPerformed(evt);
            }
        });

        cancel.setText("Cancel");
        cancel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout areYouSureLayout = new javax.swing.GroupLayout(areYouSure.getContentPane());
        areYouSure.getContentPane().setLayout(areYouSureLayout);
        areYouSureLayout.setHorizontalGroup(
            areYouSureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, areYouSureLayout.createSequentialGroup()
                .addContainerGap(52, Short.MAX_VALUE)
                .addGroup(areYouSureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(areYouSureLayout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addComponent(jLabel1))
                    .addGroup(areYouSureLayout.createSequentialGroup()
                        .addComponent(yesSave, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(noDontSave, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(48, 48, 48))
        );
        areYouSureLayout.setVerticalGroup(
            areYouSureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(areYouSureLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(areYouSureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(yesSave)
                    .addComponent(noDontSave)
                    .addComponent(cancel))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setMaximumSize(new java.awt.Dimension(200, 650));
        setMinimumSize(new java.awt.Dimension(200, 650));
        setPreferredSize(new java.awt.Dimension(200, 650));
        setLayout(null);

        exit.setText("Exit");
        exit.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                exitActionPerformed(evt);
            }
        });
        add(exit);
        exit.setBounds(12, 611, 176, 27);

        blackTimeBox.setBorder(javax.swing.BorderFactory.createTitledBorder("Black Time left"));

        blackTimeLeft.setEditable(false);
        blackTimeLeft.setBackground(new java.awt.Color(1, 1, 1));
        blackTimeLeft.setFont(new java.awt.Font("3270Narrow Nerd Font Mono", 1, 40)); // NOI18N
        blackTimeLeft.setForeground(new java.awt.Color(43, 176, 43));
        blackTimeLeft.setText("15:30");
        blackTimeLeft.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                blackTimeLeftActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout blackTimeBoxLayout = new javax.swing.GroupLayout(blackTimeBox);
        blackTimeBox.setLayout(blackTimeBoxLayout);
        blackTimeBoxLayout.setHorizontalGroup(
            blackTimeBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, blackTimeBoxLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(blackTimeLeft, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                .addContainerGap())
        );
        blackTimeBoxLayout.setVerticalGroup(
            blackTimeBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(blackTimeBoxLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(blackTimeLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        add(blackTimeBox);
        blackTimeBox.setBounds(22, 12, 160, 155);

        whiteTimeBox.setBorder(javax.swing.BorderFactory.createTitledBorder("White Time left"));

        whiteTimeLeft.setEditable(false);
        whiteTimeLeft.setBackground(new java.awt.Color(1, 1, 1));
        whiteTimeLeft.setFont(new java.awt.Font("3270Narrow Nerd Font Mono", 1, 40)); // NOI18N
        whiteTimeLeft.setForeground(new java.awt.Color(43, 176, 43));
        whiteTimeLeft.setText(" N/A");
        whiteTimeLeft.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                whiteTimeLeftActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout whiteTimeBoxLayout = new javax.swing.GroupLayout(whiteTimeBox);
        whiteTimeBox.setLayout(whiteTimeBoxLayout);
        whiteTimeBoxLayout.setHorizontalGroup(
            whiteTimeBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, whiteTimeBoxLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(whiteTimeLeft, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                .addContainerGap())
        );
        whiteTimeBoxLayout.setVerticalGroup(
            whiteTimeBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(whiteTimeBoxLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(whiteTimeLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        add(whiteTimeBox);
        whiteTimeBox.setBounds(22, 431, 160, 155);

        CurrentTurn.setBorder(javax.swing.BorderFactory.createTitledBorder("Current turn"));

        jButton2.setEnabled(false);
        jButton2.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CurrentTurnLayout = new javax.swing.GroupLayout(CurrentTurn);
        CurrentTurn.setLayout(CurrentTurnLayout);
        CurrentTurnLayout.setHorizontalGroup(
            CurrentTurnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CurrentTurnLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                .addContainerGap())
        );
        CurrentTurnLayout.setVerticalGroup(
            CurrentTurnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CurrentTurnLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
                .addContainerGap())
        );

        add(CurrentTurn);
        CurrentTurn.setBounds(20, 230, 160, 150);
    }// </editor-fold>//GEN-END:initComponents

    private void exitActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_exitActionPerformed
    {//GEN-HEADEREND:event_exitActionPerformed
	areYouSure.setVisible(true);
    }//GEN-LAST:event_exitActionPerformed

    private void noDontSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_noDontSaveActionPerformed
    {//GEN-HEADEREND:event_noDontSaveActionPerformed
	MainWindow.getWindow().removeAll();
	MainWindow.getWindow().dispose();
	areYouSure.setVisible(false);
	areYouSure.dispose();
	MainWindow.drawWindow(gameBoard);
    }//GEN-LAST:event_noDontSaveActionPerformed

    private void yesSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_yesSaveActionPerformed
    {//GEN-HEADEREND:event_yesSaveActionPerformed
	try
	{

	    JFileChooser saveFile = new JFileChooser();
	    File file;
	    saveFile.showSaveDialog(areYouSure);
	    if (saveFile.getSelectedFile() != null)
	    {
		file = saveFile.getSelectedFile();
		IO.writeGameToFile(file.getAbsolutePath(), this, gameBoard);
		MainWindow.drawWindow(gameBoard);
	    }
	    areYouSure.setVisible(false);
	    areYouSure.dispose();
	}
	catch (IOException ex)
	{
	    Logger.getLogger(InGameBar.class.getName()).log(Level.SEVERE, null, ex);
	}
    }//GEN-LAST:event_yesSaveActionPerformed

    private void cancelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cancelActionPerformed
    {//GEN-HEADEREND:event_cancelActionPerformed
	areYouSure.setVisible(false);
	areYouSure.dispose();
    }//GEN-LAST:event_cancelActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton2ActionPerformed
    {//GEN-HEADEREND:event_jButton2ActionPerformed
	// TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void blackTimeLeftActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_blackTimeLeftActionPerformed
    {//GEN-HEADEREND:event_blackTimeLeftActionPerformed
	// TODO add your handling code here:
    }//GEN-LAST:event_blackTimeLeftActionPerformed

    private void whiteTimeLeftActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_whiteTimeLeftActionPerformed
    {//GEN-HEADEREND:event_whiteTimeLeftActionPerformed
	// TODO add your handling code here:
    }//GEN-LAST:event_whiteTimeLeftActionPerformed

    private String printTime(int rawSeconds)
    {
	String time;
	int minutes = rawSeconds / 60;
	int seconds = rawSeconds % 60;
	time = (String.valueOf(minutes) + ":" + String.valueOf(seconds));
	return time;
    }

    /**
     * Timer for each player move.
     */
    @Override
    public void run()
    {
	while (whiteClock > 0 && blackClock > 0)
	{
	    try
	    {
		Thread.sleep(1000);
	    }
	    catch (InterruptedException e)
	    {
	    }
	    if (Status.isWhiteTurn())
	    {
		whiteClock--;
		whiteTimeLeft.setText(printTime(whiteClock));
	    }
	    else
	    {
		blackClock--;
		blackTimeLeft.setText(printTime(blackClock));
	    }
	}
	if (whiteClock <= 0)
	{
	    Game.timeOver(false, gameBoard);

	}
	else
	{
	    Game.timeOver(true, gameBoard);
	}
    }

    /**
     * Return white player's time left.
     *
     * @return white player's time left
     */
    public int getWhiteClock()
    {
	return whiteClock;
    }

    /**
     * Sets white player's time left.
     *
     * @param whiteClock sets white player's time left
     */
    public void setWhiteClock(int whiteClock)
    {
	this.whiteClock = whiteClock;
    }

    /**
     * Return black player's time left.
     *
     * @return black player's time left
     */
    public int getBlackClock()
    {
	return blackClock;
    }

    /**
     * Sets black player's time left.
     *
     * @param blackClock black player's time left
     */
    public void setBlackClock(int blackClock)
    {
	this.blackClock = blackClock;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel CurrentTurn;
    private javax.swing.JDialog areYouSure;
    private javax.swing.JPanel blackTimeBox;
    private javax.swing.JTextField blackTimeLeft;
    private javax.swing.JButton cancel;
    private javax.swing.JButton exit;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton noDontSave;
    private javax.swing.JPanel whiteTimeBox;
    private javax.swing.JTextField whiteTimeLeft;
    private javax.swing.JButton yesSave;
    // End of variables declaration//GEN-END:variables
}
