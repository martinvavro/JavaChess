/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.pjv.FrontEnd;

import cz.cvut.fel.pjv.GameEngine.Board;
import cz.cvut.fel.pjv.GameEngine.Figure;
import cz.cvut.fel.pjv.GameEngine.Status;
import java.util.ArrayList;
import java.util.Random;

/**
 * Draws Pawn's promotion move dialog
 *
 * @author vior
 *
 */
public class PawnPromotion extends javax.swing.JDialog
{
    private Integer changeFigureID;
    private final boolean black;
    private final int position;
    private final Board gameBoard;

    /**
     * Creates new form DrawChangeDialog
     *
     * @param pos position of the figure
     * @param white color of the figure
     */
    public PawnPromotion(int pos, boolean white, Board gameBoard)
    {
	this.gameBoard = gameBoard;
	black = white;
	position = pos;
	initComponents();
	this.setResizable(false);
	if (Status.isAiGame() && !Status.isWhiteTurn())
	{
	    ArrayList<Integer> possibleFigures = new ArrayList<>();
	    setModal(false);
	    possibleFigures.add(1);
	    possibleFigures.add(2);
	    possibleFigures.add(3);
	    possibleFigures.add(4);
	    Random r = new Random();
	    changeFigureID = (possibleFigures.get(r.nextInt(possibleFigures.size())));
	    swapChangeFigure();
	    dispose();
	}
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jPanel1 = new javax.swing.JPanel();
        rook = new javax.swing.JButton();
        queen = new javax.swing.JButton();
        bishop = new javax.swing.JButton();
        knight = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Change Figure");
        setAlwaysOnTop(true);
        setBackground(new java.awt.Color(183, 183, 183));
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setMaximumSize(new java.awt.Dimension(326, 114));
        setMinimumSize(new java.awt.Dimension(326, 114));
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(326, 114));
        setResizable(false);
        getContentPane().setLayout(new java.awt.FlowLayout());

        jPanel1.setBackground(new java.awt.Color(170, 170, 170));
        jPanel1.setMaximumSize(new java.awt.Dimension(326, 114));
        jPanel1.setMinimumSize(new java.awt.Dimension(326, 114));
        jPanel1.setName("Change Figure"); // NOI18N
        jPanel1.setOpaque(false);

        rook.setBackground(new java.awt.Color(254, 254, 254));
        rook.setForeground(new java.awt.Color(254, 254, 254));
        rook.setIcon(new javax.swing.ImageIcon(getClass().getResource("/RookWhite.png"))); // NOI18N
        rook.setToolTipText("");
        rook.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        rook.setBorderPainted(false);
        rook.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        rook.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                rookActionPerformed(evt);
            }
        });

        queen.setBackground(new java.awt.Color(254, 254, 254));
        queen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/QueenWhite.png"))); // NOI18N
        queen.setToolTipText("");
        queen.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        queen.setBorderPainted(false);
        queen.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                queenActionPerformed(evt);
            }
        });

        bishop.setBackground(new java.awt.Color(254, 254, 254));
        bishop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/BishopWhite.png"))); // NOI18N
        bishop.setToolTipText("");
        bishop.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        bishop.setBorderPainted(false);
        bishop.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                bishopActionPerformed(evt);
            }
        });

        knight.setBackground(new java.awt.Color(254, 254, 254));
        knight.setIcon(new javax.swing.ImageIcon(getClass().getResource("/KnightWhite.png"))); // NOI18N
        knight.setToolTipText("");
        knight.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        knight.setBorderPainted(false);
        knight.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                knightActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bishop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(queen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(rook, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(knight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(knight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rook, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bishop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(queen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void knightActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_knightActionPerformed
    {//GEN-HEADEREND:event_knightActionPerformed
	changeFigureID = 2;
	swapChangeFigure();
	dispose();
    }//GEN-LAST:event_knightActionPerformed

    private void bishopActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_bishopActionPerformed
    {//GEN-HEADEREND:event_bishopActionPerformed
	changeFigureID = 3;
	swapChangeFigure();
	dispose();
    }//GEN-LAST:event_bishopActionPerformed

    private void queenActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_queenActionPerformed
    {//GEN-HEADEREND:event_queenActionPerformed
	changeFigureID = 4;
	swapChangeFigure();
	dispose();
    }//GEN-LAST:event_queenActionPerformed

    private void rookActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_rookActionPerformed
    {//GEN-HEADEREND:event_rookActionPerformed
	changeFigureID = 1;
	swapChangeFigure();
	dispose();
    }//GEN-LAST:event_rookActionPerformed

    /**
     * Returns id of a figure to change.
     * @return id of a figure to change
     */
    public Integer getChangeFigureID()
    {
	return changeFigureID;
    }

    private void swapChangeFigure()
    {
	Figure newFigure = new Figure(position, changeFigureID, black);
	gameBoard.populateBoard(newFigure);
	DrawBoard.redrawBoard(gameBoard);

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bishop;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton knight;
    private javax.swing.JButton queen;
    private javax.swing.JButton rook;
    // End of variables declaration//GEN-END:variables
}
