package cz.cvut.fel.pjv.FrontEnd;

import cz.cvut.fel.pjv.GameEngine.Board;
import cz.cvut.fel.pjv.GameEngine.Loop;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 * Each button pressed on the board gets handled here during game.
 *
 * @author vior
 */
public class ButtonHandler extends AbstractAction
{

    private static int buttonPressed;
    private InGameBar ig;
    private final Loop loop;
    private Board gameBoard;

    /**
     * Constructor for ButtonHandler, creates new Loop instance.
     */
    public ButtonHandler(Board gameBoard)
    {
	this.loop = new Loop();
	this.gameBoard = gameBoard;
	
    }

    /**
     * Used for changing color of current turn in InGameMenu.
     *
     * @param igx for changing color of current turn in InGameMenu
     */
    public void setIngameMenu(InGameBar igx)
    {
	ig = igx;
    }

    /**
     * Returns InGameBar for changing color of current turn.
     *
     * @return returns InGameBar for changing color of current turn
     */
    public InGameBar getIngameMenu()
    {
	return ig;
    }

    /**
     * Returns last tile pressed on the Chess board.
     *
     * @return last tile pressed on the Chess board
     */
    public static int getButtonPressed()
    {
	return buttonPressed;
    }

    /**
     * Runs game loop.
     *
     * @param e for identifying pressed button coordinates
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {
//	This will get the pressed button coordinates
	buttonPressed = Integer.valueOf(e.getActionCommand());
//	This will run the game main loop
	loop.loop(ig, gameBoard);
    }

    /**
     * Returns current game Loop.
     * @return current game Loop
     */
    public Loop getLoop()
    {
	return loop;
    }
    
}
