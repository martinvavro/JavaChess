package cz.cvut.fel.pjv.FrontEnd;

import cz.cvut.fel.pjv.GameEngine.Board;
import java.awt.BorderLayout;
import java.io.IOException;
import javax.swing.JFrame;

/**
 * Spawns main window and panels inside it.
 * @author vior
 */
public class MainWindow extends JFrame
{

    private static MainWindow window;
    private static Board gameBoard;
    private static GameForm gform;

    /**
     * Draws main window in the center of the screen with main menu.
     * @param gameBoard
     */
    public static void drawWindow(Board gameBoard)
    {
	MainWindow.gameBoard = gameBoard;
	gform = new GameForm(gameBoard);
	window = new MainWindow();
	window.setSize(800, 650);
	window.setResizable(false);
	window.setVisible(true);
	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	window.setTitle("Chess - Main Menu");
	window.setLocationRelativeTo(null);
	window.add(new MainMenu(gameBoard));
	window.setVisible(true);
    }

    /**
     * Draws chess board and IngameStatus bar for a game.
     *
     * @param title title for the window
     * @param ig InGameBar bar
     * @throws IOException because DrawBoard accesses files.
     */
    public static void drawBoard(String title, InGameBar ig) throws IOException
    {
	window.setTitle(title);
	window.add(new DrawBoard(ig, true, gameBoard));
	window.add(ig, BorderLayout.EAST);
    }

    /**
     * Draws chess board and SetGameBar for figure layout editing.
     *
     * @param title title for the window
     * @throws IOException because DrawBoard accesses files.
     */
    public static void drawSetBoard(String title) throws IOException
    {
	window.setTitle(title);
	DrawBoard board = new DrawBoard(null, false, gameBoard);
	window.add(board);
	window.add(new SetGameBar(board, gameBoard), BorderLayout.EAST);
    }

    /**
     * Draws new game form for customizing new game options.
     *
     * @param title title for the window
     */
    public static void drawNewGameForm(String title)
    {
	window.setTitle(title);
	window.add(gform);
	gform.setVisible(true);
    }

    /**
     * Returns GameFormm so that options previously made before accessing set
     * board can be remembered.
     *
     * @return GameForm
     */
    public static GameForm getGform()
    {
	return gform;
    }

    /**
     * Returns MainWindow
     *
     * @return MainWindow
     */
    public static MainWindow getWindow()
    {
	return window;
    }

}
