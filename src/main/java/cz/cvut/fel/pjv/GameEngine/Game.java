package cz.cvut.fel.pjv.GameEngine;

import cz.cvut.fel.pjv.FrontEnd.GameMessage;
import cz.cvut.fel.pjv.FrontEnd.MainWindow;
import cz.cvut.fel.pjv.Moves.Moves;

import java.util.ArrayList;

/**
 * Contains multiple game operations.
 *
 * @author vior
 */
public class Game
{

    private int kingPosition;
    private final boolean kingInCheck = false;
    private final ArrayList<Integer> enemyMoves = new ArrayList<>();
    private final ArrayList<Integer> possibleMoves = new ArrayList<>();
    private final ArrayList<Integer> possiblePositions = new ArrayList<>();
    private static Pgn pgn = new Pgn();

    /**
     * Determines where king is located.
     *
     * @param gameBoard current board with figure locations
     */
    public void setKing(Board gameBoard)
    {
	gameBoard.getBoard().forEach((Figure figure) ->
	{
	    if (figure != null)
	    {
		if (figure.getFigureID() == 5 && figure.isWhite() == Status.isWhiteTurn())
		{
		    kingPosition = figure.getPosition();
		}
	    }
	});
    }

    /**
     * Calculates all opponent moves.
     *
     * @param gameBoard current board with figure locations
     * @return whether current board player on turn is in check
     *
     */
    public boolean calculateCheck(Board gameBoard)
    {
	boolean previouslyCheck = Status.isCheck();
	Status.setCheck(false);
	boolean check = false;
	for (Figure enemyFigure : gameBoard.getBoard())
	{
	    if (enemyFigure != null)
	    {
		if (enemyFigure.isWhite() != Status.isWhiteTurn())
		{
		    enemyMoves.removeAll(enemyMoves);
		    enemyFigure.move.calculateMoves(gameBoard, enemyFigure.getPosition());
		    enemyMoves.addAll(enemyFigure.move.getAttacks());
		    if (enemyMoves.contains(kingPosition))
		    {
			check = true;
		    }
		}
	    }
	}
	if (Moves.isJustCheck())
	    Status.setCheck(previouslyCheck);
	else
	    Status.setCheck(check);
	return check;
    }

    /**
     * Calculates all moves of current player on turn.
     */
    public void calculateThisTurnMoves(Board gameBoard)
    {
	possibleMoves.removeAll(possibleMoves);
	possiblePositions.removeAll(possiblePositions);
	gameBoard.getBoard().forEach((Figure figure) ->
	{
	    if (figure != null)
	    {
		if (figure.isWhite() == Status.isWhiteTurn())
		{
		    figure.move.setJustCheck(false);
		    figure.move.calculateMoves(gameBoard, figure.getPosition());
		    possibleMoves.addAll(figure.move.getMoves());
		    possibleMoves.addAll(figure.move.getAttacks());
		    if (!figure.move.getMoves().isEmpty() || !figure.move.getAttacks().isEmpty())
		    {
			possiblePositions.add(figure.getPosition());
		    }
		}
	    }
	});
    }

    /**
     * Deletes all moves at the end of a turn.
     */
    public void deleteAllMoves(Board gameBoard)
    {
	gameBoard.getBoard().forEach((Figure figure) ->
	{
	    if (figure != null)
	    {
		figure.move.cleanAll();
	    }
	});
    }

    /**
     * Checks whether there are any possible moves for current player on turn.
     *
     * @param pgn for printing winner info in log
     * @param gameBoard for GUI
     * @return true if game has concluded into checkmate or stalemate
     *
     */
    public boolean isGameOver(Pgn pgn, Board gameBoard)
    {
	GameMessage message;
	String text;
	Game.pgn = pgn;
	if (possibleMoves.isEmpty())
	{
	    message = new GameMessage(MainWindow.getWindow(), true, gameBoard, pgn);
	    message.setModal(true);
	    if (Status.isCheck())
	    {
		if (!Status.isWhiteTurn())
		{
		    text = "White Wins.";
		    pgn.convertCheckmateToPGN(true,false);
		}
		else
		{
		    text = "Black Wins.";
		    pgn.convertCheckmateToPGN(false,false);

		}
		message.printMessage("Checkmate! " + text);
		message.setVisible(true);
	    }
	    else
	    {
		pgn.convertStalemateToPGN();
		message.printMessage("Stalemate!");
		message.setVisible(true);
	    }
	    return true;
	}
	else if (Status.getFiftyMoveRule() > 49)
	{
	    pgn.convertStalemateToPGN();
	    message = new GameMessage(MainWindow.getWindow(), true, gameBoard, pgn);
	    message.setModal(true);
	    message.printMessage("Stalemate!");
	    message.setVisible(true);
	    return true;
	}
	return false;
    }

    /**
     * Prints dialog informing the player about game over.
     *
     * @param whitePlayer parameter for winning plater.
     * @param gameBoard for GUI
     */
    public static void timeOver(boolean whitePlayer, Board gameBoard)
    {
	GameMessage message;
	String text;
	message = new GameMessage(MainWindow.getWindow(), true, gameBoard, pgn);
	if (whitePlayer)
	{
	    text = "White Wins.";
	    pgn.convertCheckmateToPGN(true,true);
	}
	else
	{
	    text = "Black Wins.";
	    pgn.convertCheckmateToPGN(false,true);
	}
	message.printMessage("Time over! " + text);
	message.setVisible(true);
    }

    /**
     * Returns all possible positions of this turn, used for AI.
     *
     * @return ArrayList containing all possible positions.
     */
    public ArrayList<Integer> getPossiblePositions()
    {
	return possiblePositions;
    }

}
