/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.pjv.GameEngine;

import cz.cvut.fel.pjv.FrontEnd.InGameBar;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Saves and loads game.
 * @author vior
 */
public class IO
{
    
    private static BufferedReader BufferedReader;
    
    /**
     * Saves current opened game into file.
     * @param path were to save the file
     * @param inGameBar to get current time left for both players
     * @throws IOException in case path is wrong
     */
    public static void writeGameToFile(String path, InGameBar inGameBar, Board gameBoard) throws IOException
    {
	File f = new File(path);
	f.getParentFile().mkdirs();
	f.createNewFile();
	try (BufferedWriter writer = new BufferedWriter(new FileWriter(f)))
	{
	    String currentLine = Status.isWhiteTurn() ? "white" : "black";
	    writeNewLine(writer, currentLine);
	    currentLine = Status.isChessClock() ? "timed" : "notTimed";
	    writeNewLine(writer, currentLine);
	    currentLine = String.valueOf(inGameBar.getWhiteClock());
	    writeNewLine(writer, currentLine);
	    currentLine = String.valueOf(inGameBar.getBlackClock());
	    writeNewLine(writer, currentLine);
	    for (Figure figure : gameBoard.getBoard())
	    {
		if (figure != null)
		{
		    currentLine = figure.isWhite() ? "white" : "black";
		    writeNewLine(writer, currentLine);
		    currentLine = String.valueOf(figure.getPosition());
		    writeNewLine(writer, currentLine);
		    currentLine = String.valueOf(figure.getFigureID());
		    writeNewLine(writer, currentLine);
		    currentLine = figure.move.isHasMoved() ? "yes" : "no";
		    writeNewLine(writer, currentLine);
		}
	    }
	    writeNewLine(writer, "END");
	    writer.close();
	}
    }
    
    private static void writeNewLine(BufferedWriter writer, String line) throws IOException
    {
	writer.write(line + "\n");
    }
    
    /**
     * Loads game from file
     * @param f file to load
     * @param ig to pass information about time left for each player
     * @return true if file was successfully loaded
     * @throws FileNotFoundException in case file path is wrong
     * @throws IOException in case something goes wrong
     */
    public static boolean setGameFromFile(File f, InGameBar ig, Board gameBoard) throws FileNotFoundException, IOException
    {
	try (BufferedReader reader = new BufferedReader(new FileReader(f)))
	{
	    gameBoard.deleteBoard();
	    String currentLine = reader.readLine();
	    if ("white".equals(currentLine))
	    {
		Status.setWhiteTurn(true, ig);
	    }
	    else if ("black".equals(currentLine))
	    {
		Status.setWhiteTurn(false, ig);
	    }
	    currentLine = reader.readLine();
	    if ("timed".equals(currentLine))
	    {
		Status.setChessClock(true);
		currentLine = reader.readLine();
		ig.setClock();
		ig.setWhiteClock(Integer.valueOf(currentLine));
		currentLine = reader.readLine();
		ig.setBlackClock(Integer.valueOf(currentLine));
		
	    }
	    else
	    {
		Status.setChessClock(false);
		ig.setClock();
		reader.readLine();
		reader.readLine();
	    }
	    currentLine = reader.readLine();
	    while (!currentLine.equals("END"))
	    {
		boolean isWhite = "white".equals(currentLine);
		currentLine = reader.readLine();
		int position = Integer.valueOf(currentLine);
		currentLine = reader.readLine();
		int id = Integer.valueOf(currentLine);
		currentLine = reader.readLine();
		boolean moved = "yes".equals(currentLine);
		Figure figure = new Figure(position, id, isWhite);
		figure.move.setHasMoved(moved);
		gameBoard.populateBoard(figure);
		currentLine = reader.readLine();
	    }
	    return true;
	}
	catch (java.lang.NumberFormatException exception)
	{
	    Logger log = Logger.getLogger("Error log");
	    log.setLevel(Level.ALL);
	    log.severe("Wrong file format!");
	    return false;    
	}
	catch (java.lang.NullPointerException exception)
	{
	    Logger log = Logger.getLogger("Error log");
	    log.setLevel(Level.ALL);
	    log.warning("No file selected!");
	    return false;  
	}
    }
    
}
