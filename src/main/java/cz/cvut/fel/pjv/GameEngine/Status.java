package cz.cvut.fel.pjv.GameEngine;

import cz.cvut.fel.pjv.FrontEnd.InGameBar;

/**
 * Keeps track of current game status. *
 * @author vior
 * 
 */
public class Status
{
    
    private static boolean check;
    private static boolean whiteTurn = true;
    private static int fiftyMoveRule = 0;
    private static boolean ChessClock;
    private static int timeLimit;
    private static boolean aiGame;

    /**
     * Returns true if white player's turn, false for otherwise.
     * @return true for white player's turn false for otherwise
     */
    public static boolean isWhiteTurn()
    {
	return whiteTurn;
    }

    /**
     * Returns whether game is timed.
     * @return whether game is timed
     */
    public static boolean isChessClock()
    {
	return ChessClock;
    }

    /**
     * Sets whether game is timed.
     * @param ChessClock whether game is timed
     */
    public static void setChessClock(boolean ChessClock)
    {
	Status.ChessClock = ChessClock;
    }


    /**
     * Changes player's turn.
     * @param whiteTurn true for white player's turn false for otherwise
     * @param ig passed for switching color on a IngameStatus bar
     */
    public static void setWhiteTurn(boolean whiteTurn, InGameBar ig)
    {
	Status.whiteTurn = whiteTurn;
	ig.setBackGround(whiteTurn);
    }

    /**
     * Returns check.
     * @return true if a current player's king is in check
     */
    public static boolean isCheck()
    {
	return check;
    }

    /**
     * Sets check.
     * @param check true if a current player's king is in check
     */
    public static void setCheck(boolean check)
    {
	Status.check = check;
    }

    /**
     * Returns number of moves made without any committed attack.
     * @return number of moves made without any committed attack.
     */
    public static int getFiftyMoveRule()
    {
	return fiftyMoveRule;
    }

    /**
     * Adds move made.
     */
    public static void addFiftyMoveRule()
    {
	fiftyMoveRule += 1;
    }
    
    /**
     * Resets the fifty move rule counter.
     */
    public static void resetFiftyMoveRule()
    {
	fiftyMoveRule = 0;
    }

    /**
     * Sets the fiftyMoveRule to a number in parameter.
     * Used when calculating check.
     * @param fiftyMoveRule sets the fiftyMoveRule
     */
    public static void setFiftyMoveRule(int fiftyMoveRule)
    {
	Status.fiftyMoveRule = fiftyMoveRule;
    }

    /**
     * Returns time set at the beginning of game.
     * @return time set at the beginning of game
     */
    public static int getTimeLimit()
    {
	return timeLimit;
    }

    /**
     * Sets time limit for game.
     * @param timeLimit time limit for this game
     */
    public static void setTimeLimit(int timeLimit)
    {
	Status.timeLimit = timeLimit;
    }

    /**
     * Returns true if aiGame was set.
     * @return true if aiGame was set
     */
    public static boolean isAiGame()
    {
	return aiGame;
    }

    /**
     * True if aiGame was set.
     * @param isAiGame True if aiGame was set.
     */
    public static void setAiGame(boolean isAiGame)
    {
	Status.aiGame = isAiGame;
    }

    
    
}
