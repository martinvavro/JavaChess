/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.pjv.GameEngine;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Logs all moves in PGN form.
 *
 * @author vior
 */
public class Pgn
{

    private int round;
    private String whitePGN = " ";
    private String blackPGN = " ";
    private Logger log;
    String pgn;

    /**
     * Constructor for PGN, creates new log.
     */
    public Pgn()
    {
	round = 0;
	log = Logger.getLogger("PGN log");
	log.setLevel(Level.ALL);
	pgn = "";
    }
    
    public Pgn(int round, String pgn)
    {
	this.round = round;
	log = Logger.getLogger("PGN log");
	log.setLevel(Level.ALL);
	this.pgn = pgn;
    }

    /**
     * Combines both white and black PGN record along with round number.
     */
    public void logPGN()
    {
	round++;
	String thisRound = String.valueOf(round) + ". ";
	if (!"".equals(whitePGN))
	{
	    thisRound += whitePGN;
	}
	if (!"".equals(blackPGN))
	{
	    thisRound += blackPGN;
	}
	if (!"".equals(blackPGN) || !"".equals(whitePGN))
	{
	    log.info(thisRound);
	    pgn += thisRound;
	}
	whitePGN = "";
	blackPGN = "";
    }

    /**
     * Gets this round white player move.
     *
     * @param figure is needed to get round information.
     */
    public void whitePGN(Figure figure)
    {
	if (figure.move.isLeftCastleCommited() || figure.move.isRightCastleCommited())
	{
	    whitePGN = convertCastlingToPGN(figure);
	}
	else
	{
	    if (!figure.move.isPawnPromotion())
	    {
		whitePGN = convertPieceTypeToPGN(figure);
	    }
	    else
	    {
		whitePGN = "";
	    }
	    if (figure.move.isWasAttack())
	    {
		whitePGN += "x";
	    }
	    whitePGN += convertLocationToPGN(figure.getPosition());
	    if (figure.move.isPawnPromotion())
	    {
		whitePGN = removeLastChar(whitePGN);
		whitePGN += "=" + convertPieceTypeToPGN(figure) + " ";
	    }
	    if (Status.isCheck())
	    {
		whitePGN = removeLastChar(whitePGN);
		whitePGN += "+ ";
	    }
	}
    }

    /**
     * Gets this round black player move.
     *
     * @param figure is needed to get round information.
     */
    public void blackPGN(Figure figure)
    {
	if (figure.move.isLeftCastleCommited() || figure.move.isRightCastleCommited())
	{
	    blackPGN = convertCastlingToPGN(figure);
	}
	else
	{
	    if (!figure.move.isPawnPromotion())
	    {
		blackPGN = convertPieceTypeToPGN(figure);
	    }
	    else
	    {
		blackPGN = "";
	    }
	    if (figure.move.isWasAttack())
	    {
		blackPGN += "x";
	    }
	    blackPGN += convertLocationToPGN(figure.getPosition());
	    if (figure.move.isPawnPromotion())
	    {
		blackPGN = removeLastChar(blackPGN);
		blackPGN += "=" + convertPieceTypeToPGN(figure) + " ";
	    }
	    if (Status.isCheck())
	    {
		blackPGN = removeLastChar(blackPGN);
		blackPGN += "+ ";
	    }
	}
    }

    /**
     * Prints PGN log when checkmate happens.
     *
     * @param white true if white player won, false for otherwise
     * @param clockRunOut whether game was won by clock
     */
    public void convertCheckmateToPGN(boolean white, boolean clockRunOut)
    {
	if (white)
	{
	    if (!clockRunOut)
	    {
		whitePGN = removeLastChar(whitePGN);
		whitePGN = removeLastChar(whitePGN);
		whitePGN += "#";
	    }
	    logPGN();
	    log.info("1-0");
	}
	else
	{
	    if (!clockRunOut)
	    {
		blackPGN = removeLastChar(blackPGN);
		blackPGN = removeLastChar(blackPGN);
		blackPGN += "#";
	    }
	    logPGN();
	    log.info("0-1");
	}
    }

    /**
     * Prints PGN log when stalemate happens.
     */
    public void convertStalemateToPGN()
    {
	logPGN();
	log.info("1/2-1/2");
    }

    private String convertLocationToPGN(int location)
    {
	String pgnLocation = null;
	switch (location % 10)
	{
	    case 1:
		pgnLocation = "a";
		break;
	    case 2:
		pgnLocation = "b";
		break;
	    case 3:
		pgnLocation = "c";
		break;
	    case 4:
		pgnLocation = "d";
		break;
	    case 5:
		pgnLocation = "e";
		break;
	    case 6:
		pgnLocation = "f";
		break;
	    case 7:
		pgnLocation = "g";
		break;
	    case 8:
		pgnLocation = "h";
		break;
	    default:
		break;
	}
	pgnLocation += String.valueOf(location / 10);
	pgnLocation += " ";
	return pgnLocation;
    }

    private static String convertPieceTypeToPGN(Figure figure)
    {
	String pgnType = null;
	if (null != figure.getFigureID())
	{
	    switch (figure.getFigureID())
	    {
		case 0:
		    pgnType = "";
		    break;
		case 1:
		    pgnType = "R";
		    break;
		case 2:
		    pgnType = "N";
		    break;
		case 3:
		    pgnType = "B";
		    break;
		case 4:
		    pgnType = "Q";
		    break;
		case 5:
		    pgnType = "K";
		    break;
		default:
		    break;
	    }
	}
	return pgnType;
    }

    private static String convertCastlingToPGN(Figure figure)
    {
	String pgnCastle = "";
	if (figure.move.isLeftCastleCommited())
	{
	    pgnCastle = "O-O-O ";
	}
	else if (figure.move.isRightCastleCommited())
	{
	    pgnCastle = "O-O ";
	}
	return pgnCastle;
    }

    private static String removeLastChar(String str)
    {
	return str.substring(0, str.length() - 1);
    }

    public String getPgn()
    {
	return pgn;
    }
    
}
