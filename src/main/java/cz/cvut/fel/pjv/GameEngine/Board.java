/*
 * Copyright (C) 2018 vior
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package cz.cvut.fel.pjv.GameEngine;

import java.util.ArrayList;

/**
 * ArrayList which stores Figure on corresponding position.
 *
 * @author vior
 *
 */
public final class Board
{

    private boolean whiteKing;
    private boolean blackKing;
    private ArrayList<Figure> board;

    public Board()
    {
	this.board = new ArrayList<>(100);
	createBoard();
    }

    /**
     * Creates empty board.
     */
    private void createBoard()
    {
	for (int i = 0; i < 100; i++)
	{
	    board.add(i, null);
	}
    }

    /**
     * Deep clones Board object.
     * @return cloned board
     */
    public Board cloneBoard()
    {
	Board b = new Board();
	ArrayList<Figure> boardClone = new ArrayList<>();
	for (Figure f : this.board)
	{
	    if (f != null)
	    {
		boardClone.add(f.cloneFigure(f));
	    }
	    else
	    {
		boardClone.add(f);
	    }
	}
	b.setBoard(boardClone);
	b.setBlackKing(this.blackKing);
	b.setWhiteKing(this.whiteKing);
	return b;
    }

    /**
     * Used when creating custom figure layout.
     *
     * @return true if there are both kings present
     *
     */
    public boolean isBothKings()
    {
	whiteKing = false;
	blackKing = false;
	for (Figure figure : board)
	{
	    if (figure != null)
	    {
		if (figure.getFigureID() == 5)
		{
		    if (figure.isWhite())
		    {
			whiteKing = true;
		    }
		    else
		    {
			blackKing = true;
		    }
		}
	    }
	}
	return (blackKing && whiteKing);
    }

    /**
     * Used when creating custom figure layout.
     *
     * @return true if white king is present
     *
     */
    public boolean isWhiteKing()
    {
	return whiteKing;
    }

    /**
     * Used when creating custom figure layout.
     *
     * @return true if black king is present
     *
     */
    public boolean isBlackKing()
    {
	return blackKing;
    }

    /**
     * Creates default chess starting figure layout.
     */
    public void defaultLayout()
    {
	this.deleteBoard();
//	White figures
	this.populateBoard(new Figure(21, 0, true));
	this.populateBoard(new Figure(22, 0, true));
	this.populateBoard(new Figure(23, 0, true));
	this.populateBoard(new Figure(24, 0, true));
	this.populateBoard(new Figure(25, 0, true));
	this.populateBoard(new Figure(26, 0, true));
	this.populateBoard(new Figure(27, 0, true));
	this.populateBoard(new Figure(28, 0, true));
	this.populateBoard(new Figure(11, 1, true));
	this.populateBoard(new Figure(12, 2, true));
	this.populateBoard(new Figure(13, 3, true));
	this.populateBoard(new Figure(14, 4, true));
	this.populateBoard(new Figure(15, 5, true));
	this.populateBoard(new Figure(16, 3, true));
	this.populateBoard(new Figure(17, 2, true));
	this.populateBoard(new Figure(18, 1, true));
//	Black figures
	this.populateBoard(new Figure(71, 0, false));
	this.populateBoard(new Figure(72, 0, false));
	this.populateBoard(new Figure(73, 0, false));
	this.populateBoard(new Figure(74, 0, false));
	this.populateBoard(new Figure(75, 0, false));
	this.populateBoard(new Figure(76, 0, false));
	this.populateBoard(new Figure(77, 0, false));
	this.populateBoard(new Figure(78, 0, false));
	this.populateBoard(new Figure(81, 1, false));
	this.populateBoard(new Figure(82, 2, false));
	this.populateBoard(new Figure(83, 3, false));
	this.populateBoard(new Figure(84, 4, false));
	this.populateBoard(new Figure(85, 5, false));
	this.populateBoard(new Figure(86, 3, false));
	this.populateBoard(new Figure(87, 2, false));
	this.populateBoard(new Figure(88, 1, false));
    }

    /**
     * Finds figure at requested position.
     *
     * @param position coordinate of requested figure
     * @return figure at position
     */
    public Figure findByPosition(int position)
    {
	return board.get(position);
    }

    /**
     * Places figure on corresponding position on board.
     *
     * @param figure places figure on corresponding position on board.
     */
    public void populateBoard(Figure figure)
    {
	board.set(figure.getPosition(), new Figure(figure.getPosition(), figure.getFigureID(), figure.isWhite()));
    }

    /**
     * Used for changing figure position.
     *
     * @param position new position of a figure
     * @param figure figure to place on a new position
     *
     */
    public void changeFigure(int position, Figure figure)
    {
	if (figure != null)
	{
	    figure.setPosition(position);
	}
	board.set(position, figure);
    }

    /**
     * Switches two figures positions.
     *
     * @param ogPosition original position of a figure
     * @param position new position of a figure
     */
    public void switchPosition(int ogPosition, int position)
    {
	Figure ogPos = this.findByPosition(ogPosition);
	Figure Pos = this.findByPosition(position);

	this.changeFigure(ogPosition, Pos);
	this.changeFigure(position, ogPos);
    }

    /**
     * Moves figure winner on a position of a looser and deletes looser.
     *
     * @param winner figure which gets on a loser position
     * @param loser figure which gets deleted from board
     */
    public void attackPosition(int winner, int loser)
    {
	Figure ogPos = this.findByPosition(winner);
	Figure Pos = this.findByPosition(loser);

	this.changeFigure(loser, ogPos);
	this.changeFigure(winner, null);
    }

    /**
     * Deletes figure at requested position
     *
     * @param position deletes figure at requested position.
     */
    public void deletePosition(int position)
    {
	this.changeFigure(position, null);
    }

    /**
     * Clears board for a new game.
     */
    public void deleteBoard()
    {
	for (Figure i : board)
	{
	    if (i != null)
	    {
		deletePosition(i.getPosition());
	    }
	}
    }

    /**
     * Returns current active board.
     *
     * @return current active board
     */
    public ArrayList<Figure> getBoard()
    {
	return board;
    }

    private void setBoard(ArrayList<Figure> a)
    {
	this.board = a;
    }

    private void setWhiteKing(boolean whiteKing)
    {
	this.whiteKing = whiteKing;
    }

    private void setBlackKing(boolean blackKing)
    {
	this.blackKing = blackKing;
    }

}
