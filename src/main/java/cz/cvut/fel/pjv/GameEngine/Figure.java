/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.pjv.GameEngine;

import cz.cvut.fel.pjv.Moves.Bishop;
import cz.cvut.fel.pjv.Moves.King;
import cz.cvut.fel.pjv.Moves.Knight;
import cz.cvut.fel.pjv.Moves.Moves;
import cz.cvut.fel.pjv.Moves.Pawn;
import cz.cvut.fel.pjv.Moves.Queen;
import cz.cvut.fel.pjv.Moves.Rook;

/**
 * Bean of a figure.
 * @author vior
 *
 */
public final class Figure
{

    private Integer position;
    private Integer figureID;
    private boolean white;
    /**
     * Access to figure move operations.
     */
    public Moves move;

    /**
     * Constructor of a figure.
     *
     * @param positionID figure position
     * @param figureID figure id
     * @param White true for white, false for black
     */
    public Figure(Integer positionID, Integer figureID, boolean White)
    {
	this.move = new Moves();
	this.position = positionID;
	this.figureID = figureID;
	this.white = White;
	setFigureType();
    }

    /**
     * Constructor of a figure.
     * @param f Figure.
     *
     */
    public Figure(Figure f)
    {
	this.move = new Moves();
	this.position = f.position;
	this.figureID = f.figureID;
	this.white = f.white;
	setFigureType();
    }

    /**
     * Returns figure position.
     * @return figure position
     */
    public int getPosition()
    {
	return position;
    }

    /**
     * Deep clones Figure.
     * @param f figure to clone
     * @return cloned figure
     */
    public Figure cloneFigure(Figure f)
    {
	Figure figureClone = new Figure(f);
	return figureClone;
    }
	    
    
    /**
     * Sets figure position coordinates.
     * @param position sets figure position
     */
    public void setPosition(int position)
    {
	this.position = position;
    }

    /**
     * Returns figure ID.
     * @return figure ID
     */
    public Integer getFigureID()
    {
	return figureID;
    }

    /**
     * Sets figure ID.
     * @param figureID sets figure ID
     */
    public void setFigureID(int figureID)
    {
	this.figureID = figureID;
    }

    /**
     * Returns true if figure is white, false if otherwise.
     * @return true for a white figure false for a black one
     */
    public boolean isWhite()
    {
	return white;
    }

    /**
     * Sets figure color, true for white, false for black.
     * @param isWhite true for a white figure false for a black one
     */
    public void setWhite(boolean isWhite)
    {
	white = isWhite;
    }

    /**
     * Adds corresponding moves to figure ID.
     */
    public void setFigureType()
    {
	switch (getFigureID())
	{
	    case 0:
		move = new Pawn();
		break;
	    case 1:
		move = new Rook();
		break;
	    case 2:
		move = new Knight();
		break;
	    case 3:
		move = new Bishop();
		break;
	    case 4:
		move = new Queen();
		break;
	    case 5:
		move = new King();
		break;
	    default:
		break;
	}
    }
}
