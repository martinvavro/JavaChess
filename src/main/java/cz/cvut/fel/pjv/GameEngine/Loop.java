package cz.cvut.fel.pjv.GameEngine;

import cz.cvut.fel.pjv.FrontEnd.ButtonHandler;
import cz.cvut.fel.pjv.FrontEnd.DrawBoard;
import cz.cvut.fel.pjv.FrontEnd.InGameBar;
import java.util.ArrayList;
import java.util.Random;

/**
 * Main game loop.
 *
 * @author vior
 *
 */
public class Loop
{

    private boolean notAfirstMove;
    private Figure previousPosition;
    private Figure selectedPosition;
    private final Pgn pgn;
    private Game game;

    /**
     * Constructor for game loop, creates PGN log.
     */
    public Loop()
    {
	this.previousPosition = null;
	this.notAfirstMove = false;
	pgn = new Pgn();
	Status.resetFiftyMoveRule();
	game = new Game();
    }

    /**
     * Main game loop, switches player's turns and commits moves.
     *
     * @param ig for switching background of player's turns box in InGameBar
     */
    public void loop(InGameBar ig, Board gameBoard)
    {
	boolean isEmpty = false;

//	Calculate first moves
	if (!notAfirstMove)
	{
	    game.setKing(gameBoard);
	    game.calculateThisTurnMoves(gameBoard);
	    game.calculateCheck(gameBoard);
	    notAfirstMove = true;
	    game.isGameOver(pgn, gameBoard);
	}

//	get the pressed button coordinates figure
	selectedPosition = gameBoard.findByPosition(ButtonHandler.getButtonPressed());

//	This prevents sending commitMove to a figure which doesn't have any legal moves
	if (previousPosition != null)
	{
	    if (previousPosition.move.getMoves().isEmpty() && previousPosition.move.getAttacks().isEmpty())
	    {
		isEmpty = true;
	    }
	}
//	This is the main loop
	if (previousPosition == null || isEmpty)
	{
	    if (selectedPosition != null)
	    {
		if (selectedPosition.isWhite() == Status.isWhiteTurn())
		{
		    previousPosition = selectedPosition;
		    selectedPosition.move.drawPossibilities();
		}
	    }
	}
	else
	{
	    if (previousPosition.move.commitMove(gameBoard, ButtonHandler.getButtonPressed()))
	    {
		prepareForNextPlayer(ig, gameBoard);
	    }
	    previousPosition = null;
	    DrawBoard.redrawBoard(gameBoard);
	}
    }

    private void prepareForNextPlayer(InGameBar ig, Board gameBoard)
    {

//	switch turns
	if (Status.isWhiteTurn())
	{
	    Status.setWhiteTurn(false, ig);
	    game.setKing(gameBoard);
	    game.calculateCheck(gameBoard);
	    pgn.whitePGN(gameBoard.findByPosition(previousPosition.getPosition()));

	}
	else
	{
	    Status.setWhiteTurn(true, ig);
	    game.setKing(gameBoard);
	    game.calculateCheck(gameBoard);
	    game.deleteAllMoves(gameBoard);
	    pgn.blackPGN(gameBoard.findByPosition(previousPosition.getPosition()));

	}
//	calculates next turn moves
	
	game.calculateThisTurnMoves(gameBoard);
	notAfirstMove = true;
	DrawBoard.redrawBoard(gameBoard);
	if (!game.isGameOver(pgn, gameBoard))
	{
	    if(Status.isWhiteTurn())
		pgn.logPGN();	
//	    run ai loop if defined and if it is black player turn
	    if (Status.isAiGame())
	    {
		if (!Status.isWhiteTurn())
		{
		    aiLoop(ig, gameBoard);
		}
	    }
	}
	else
	{
	    pgn.logPGN();
	}
    }

    private void aiLoop(InGameBar ig, Board gameBoard)
    {
	Random random = new Random();
//	get random figure 
	previousPosition = gameBoard.findByPosition(game.getPossiblePositions().get(random.nextInt(game.getPossiblePositions().size())));
	ArrayList<Integer> allMoves = new ArrayList<>();
	allMoves.addAll(previousPosition.move.getAttacks());
	allMoves.addAll(previousPosition.move.getMoves());
//	commit random move 
	previousPosition.move.commitMove(gameBoard, allMoves.get(random.nextInt(allMoves.size())));
//	prepare game for next move
	DrawBoard.redrawBoard(gameBoard);
	prepareForNextPlayer(ig, gameBoard);
    }

    /**
     * Returns current game information.
     * @return current game information
     */
    public Game getGame()
    {
	return game;
    }

    /**
     * Returns current PGN instance.
     * @return current PGN instance
     */
    public Pgn getPgn()
    {
	return pgn;
    }
    

}
